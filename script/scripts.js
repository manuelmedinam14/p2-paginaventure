var indicador = 0;
var posicion = 0;
window.addEventListener('load',function(){
    document.getElementById("L").addEventListener("click",function(e){
        moveSlider('izq')
    })
    document.getElementById("R").addEventListener("click",function(e){
        moveSlider('der')
    })
    //tamaño
    definirTamaño()
})

function definirTamaño(){
    var sliders = document.querySelectorAll("#slide");
    var heiC= document.querySelector("#contenedor-sl");
    var parrafos = document.querySelectorAll("#parrafo");



    document.querySelectorAll("#C").forEach(function(el){
        el.style.width =(heiC.clientWidth)+"px";

    })
    
    parrafos.forEach(function(el,i){
        el.style.height =  `${(heiC.clientWidth * 0.5)}px`;
        el.style.width =(heiC.clientWidth/(2.6))+"px";
    })

    sliders.forEach(function(el,i){
        
        var link = el.getAttribute("data-background");
        el.style.background = `url(${link})`;
        el.style.height =  `${(heiC.clientWidth * 0.5)}px`;
        el.style.width =(heiC.clientWidth/2)+"px";
        el.style.backgroundRepeat = 'no-repeat';
        el.style.backgroundPosition = "center";
        el.style.marginLeft = "50px";
        el.style.float = "left";

    })
}


function moveSlider(direccion){
    var limite = document.querySelectorAll("#slide").length;
    indicador = ( direccion == 'der' ) ? indicador + 1 : indicador -1;    
    indicador = ( indicador >= limite ) ? 0 : indicador ;
    indicador = ( indicador < 0 ) ? limite - 1 : indicador;

    var conteneSl = document.getElementById("Sl-cotn");
    var heiC= document.querySelector("#contenedor-sl");

    conteneSl.style.marginLeft=`${-indicador*heiC.clientWidth}px`
    

    conteneSl.animate([
        // keyframes        
       { marginLeft : `${-posicion*heiC.clientWidth}px` }, 
       { marginLeft : `${-indicador*heiC.clientWidth}px` }
    ], { 
        // timing options
        duration: 800,
        iterations: 1,
  
      });
    posicion = ( direccion == 'der' ) ? posicion + 1 : posicion -1;    
    posicion = ( posicion >= limite ) ? 0 : posicion ;
    posicion = ( posicion < 0 ) ? limite - 1 : posicion;
}